package com.lowware.math;



/**
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
 
 public class Solution {
 
	
	private static int calculate() {
	
		for (int test = 20; test < Integer.MAX_VALUE; test++) {
			boolean good = true;		
			for (int i = 1; i < 21; i++) {
				if (test % i != 0) {
					good = false;
					break;
				}
			}
			if (good == true) {
				return test;
			}
		}
		return 0; //unreachable if there is a solution
	
	}
 
 
	public static void main(String[] args) {
		
	
		System.out.println("The smallest positive number evenly divisible by all of the numbers from 1 to 20 is: " + calculate());
	
	}
 
 }