package com.lowware.math;



/**
 * What is the 10001st prime number?
 */
 
 public class Solution {
 

	public static boolean isPrime(int num) {
		int sqr = (int) Math.floor(Math.sqrt(num)) + 1;
		for (int i = 2; i < sqr; i++) {
			if (num%i == 0) {
				return false;
			}
		}	
		return true;
	}
	
	public static int nthPrime(int n) {
		int nextPrime = 1;
		int count = 0;
		int i = 2;
		
		while (count < n) {
			if (isPrime(i)) {
				nextPrime = i;
				count++;
				System.out.println("Prime " + count + " is " + i);
			}
			i++;		
		}
		return nextPrime;
		
	}
	
	public static int calculate() {
		return nthPrime(10001);
	}
	
 
 
 
	public static void main(String[] args) {
		
	
		System.out.println("The 10001st prime is: " + calculate());
	
	}
 
 }