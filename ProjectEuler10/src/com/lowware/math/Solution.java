package com.lowware.math;



/**
 *  Find the sum of all the primes below two million.
 */
 
 public class Solution {

	public static boolean isPrime(int num) {
		int sqr = (int) Math.ceil(Math.sqrt(num)) + 1;
		int prime = 0;
		
		if (num < 20) {
			sqr = num - 1;
		}
		
		for (int i = 2; i <= sqr; i++) {
			if (num%i == 0) {
				return false;
			}
		}
		return true;
	
	}
	
	
	public static long calculate() {
		long total = 0;
		for (int i = 2000000; i > 1; i--) {
			if (isPrime(i)) {
				total = total + i;
			}
		}
		
		return total;
		
	}
 
 
	public static void main(String[] args) {
		
		System.out.println("The sum of all primes below two million is: " + calculate());
	
	}
 
 }