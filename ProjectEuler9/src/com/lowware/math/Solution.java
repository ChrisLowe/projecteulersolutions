package com.lowware.math;



/**
 *  Find the product of a pythagorean triplet (a^2 + b^2 = c^2) for with a+b+c = 1000
 */
 
 public class Solution {
 
	
	public static boolean isTriplet(int a, int b, int c) {
		if ((a*a + b*b) == (c*c)) {
			return true;
		}
		return false;
	}
	
	
	public static int calculate() {
		
		for (int n = 0; n < 1000; n++) {
			for (int m = n+1; m < 1000; m++) {
				int a = m*m - n*n;
				int b = 2*m*n;
				int c = m*m + n*n;

				if (a+b+c == 1000) {
					System.out.println(a + "^2 +" + b + "^2 = " + c + "^2 " + isTriplet(a, b, c));
					System.out.println(a + "+" + b + "+" + c + " = 1000");
					return a*b*c;
				}
			}
		}
		return -1;
		
	}
	
 
 
 
	public static void main(String[] args) {
		
		System.out.println("The answer is: " + calculate());
	
	}
 
 }