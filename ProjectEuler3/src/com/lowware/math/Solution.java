package com.lowware.math;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 */
 
 public class Solution {
 
	private static long calculate() {
		
		long answer = 0L;
		long num = 600851475143L;
		long count = 1L;
		long lastprime = 1L;
		
		//list all primes
		for (long l = 1; l < 775146; l++) { //Math.sqrt(num)
			if (num % l == 0) {
				System.out.println(l + " is a prime factor of " + num); 
				answer = l;
			}
		}
		return answer;
	}
		
 
 
	public static void main(String[] args) {
	
		System.out.println("The largest prime factor of 600851475143 is: " + calculate());
	
	}
 
 }