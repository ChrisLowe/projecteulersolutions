package com.lowware.math;

/**
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million, 
 * find the sum of the even-valued terms.
 */
 
 public class Solution {
 
	private static int calculate() {
		
		int total = 0;
		int max = 4000000;
		int next = 1;
		int last = 1;
		
		while (next < max) {
			int temp = next;
			next = next + last;
			last = temp;
			if (next % 2 == 0) {
				total += next;
			}
		}
		
		return total;
		
	}
 
 
	public static void main(String[] args) {
	
		long start = System.nanoTime();
		System.out.println("The sum of all even values of the fibonnaci series below four million is: " + calculate());
		long end = System.nanoTime();
		System.out.println(start + " - " + end + " = " + (end - start));
	
	}
 
 }