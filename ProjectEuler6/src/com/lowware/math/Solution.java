package com.lowware.math;



/**
 * Find the difference between the sum of the squares of the 
 * first one hundred natural numbers and the square of the sum.
 */
 
 public class Solution {
 
	private static int sumOfSquare(int to) {
		int total = 0;
		for (int i = 0; i <= to; i++) {
			total = total + (int) (Math.pow(i, 2));
		}
		return total;
	}
	
	private static int squareOfSum(int to) {
		int total = 0;
		for(int i = 0; i <= to; i++) {
			total+= i;
		}
		return total*total;
	}
	
	
	private static int calculate() {
		int sumOf = sumOfSquare(100);
		int squareOf = squareOfSum(100);
		int dif = squareOf - sumOf;
		System.out.println("Difference between " + squareOf + " and " + sumOf + " is: " + dif);
		
		
		return dif;
	
	}
 
 
	public static void main(String[] args) {
		
	
		System.out.println("The difference between the sum of the squares of the first one hundred natural numbers and the square of the sum is: " + calculate());
	
	}
 
 }