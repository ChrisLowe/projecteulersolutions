package com.lowware.math;

/**
 *Find the sum of all the multiples of 3 or 5 below 1000.
 */
 
 public class Solution {
 
	private static int calculate() {
		int total = 0;
		int max = 1000;
		for (int i = 2; i < max; i++) {
			if ((i % 3 == 0) || (i % 5 == 0)) {
				total+=i;
			}
		}
		return total;	
	}
 
 
	public static void main(String[] args) {
	
		System.out.println("The sum of all multiples of 3 or 5 below 1000 is: " + calculate());
	
	}
 
 }