package com.lowware.math;



/**
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
 
 public class Solution {
 
	//returns the reverse of num
	private static int reverse(int num) {
		StringBuffer str = new StringBuffer();
		str.append(num);
		str.reverse();
		return new Integer(str.toString()).intValue();
	}
	
	private static int calculate() {
	
		int largest = 0;
	
		for (int i = 999; i > 0; i--) {
			for (int p = 999; p > 0; p--) {
				int test = i*p;
				if (test == reverse(test)) {
					System.out.println(i + " * " + p + " = " + i*p);
					
					if (test > largest) {
						largest = test;
					}
				}
			}
		}
		return largest;	
	}
	
	
 
 
	public static void main(String[] args) {
		
	
		System.out.println("The largest palindrome of two three digit products is " + calculate());
	
	}
 
 }